# Description
console program in c language based on the code by Claude Heiland-Allen from library [mandelbrot-numerics]( http://code.mathr.co.uk/mandelbrot-numerics)

it computes [center ( nucleus) of hyperbolic componnet of Mandelbrot set ]( https://en.wikibooks.org/wiki/Fractals/Iterations_in_the_complex_plane/Mandelbrot_set/centers)
 
using:
* [Newton method ]( https://en.wikibooks.org/wiki/Fractals/Mathematics/Newton_method)
* double precision floating point numbers




# How to use ?
to compile
```c
 gcc m.c -Wall -std=c99 -lm 
```
to run 
```c
  ./a.out
```

Example output  
>>>
Newton method converged after 6 steps and  near c= ( 0.000000 ;1.000000 ) found nucleus = center = ( -0.1225611668766536, 0.7448617666197442 ) for period = 3   
Newton method converged after 5 steps and  near c= ( 0.250000 ;0.500000 ) found nucleus = center = ( 0.2822713907669139, 0.5300606175785253 ) for period = 4   
Newton method converged after 5 steps and  near c= ( 0.250000 ;-0.500000 ) found nucleus = center = ( 0.2822713907669139, -0.5300606175785253 ) for period = 4   
Newton method converged after 8 steps and  near c= ( 0.300000 ;0.300000 ) found nucleus = center = ( 0.3795135880159238, 0.3349323055974976 ) for period = 5   
Newton method converged after 5 steps and  near c= ( -0.122561 ;0.844862 ) found nucleus = center = ( -0.1134186559494366, 0.8605694725015731 ) for period = 6   
Newton method converged after 7 steps and  near c= ( -1.250000 ;0.250000 ) found nucleus = center = ( -1.1380006666509646, 0.2403324012620983 ) for period = 6   
Newton method converged after 35 steps and  near c= ( -3.250000 ;0.250000 ) found nucleus = center = ( -1.9667732163929286, -0.0000000000000000 ) for period = 6   
>>>    

# Git commands
```bash
cd existing_folder
git init
git remote add origin git@gitlab.com:adammajewski/mandelbrot-nucleus-c-double.git
git add .
git commit
git push -u origin master
```


# compare with
* https://gitlab.com/adammajewski/cpp-mandelbrot-center-by-knighty
* https://gitlab.com/adammajewski/lawrence
* https://gitlab.com/adammajewski/mandelbrot-numerics-nucleus

# to do 
* find examples that failed to converge and show why 


# License

This project is licensed under the  Creative Commons Attribution-ShareAlike 4.0 International License - see the [LICENSE.md](LICENSE.md) file for details

# technical notes
GitLab uses:
* the Redcarpet Ruby library for [Markdown processing](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
* KaTeX to render [math written with the LaTeX syntax](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md), but [only subset](https://khan.github.io/KaTeX/function-support.html)


